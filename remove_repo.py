#!/usr/bin/env python3

"""
Script to un-publish and remove a Debian repository from a Debian package repository server.
"""

import argparse
import logging
import sys
import textwrap
from aptly_api import Client
from aptly_api.base import AptlyAPIException

logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")
logger = logging.getLogger(__name__)


def parse_arguments():
    """
    Parse command-line arguments.
    :return: argparse.Namespace
    """
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent(
            """
            Un-publish and remove a Debian repository from a Debian package repository
            server, using Aptly (https://www.aptly.info). The published repository,
            identified by distribution and prefix, will be un-published, and all the local
            repositories associated with the published repository (one for each component)
            will be removed. For example:

                $ remove_repo.py -d bionic -p schaap

            This will un-publish the repository for the distribution 'bionic' with prefix
            'schaap'. All local repositories whose name start with 'schaap-bionic' will be
            removed.
            """
        ),
        # Do not add help automatically to prevent that two sections of optional arguments appear
        add_help=False,
    )
    required = parser.add_argument_group("required arguments")
    optional = parser.add_argument_group("optional arguments")
    required.add_argument(
        "-d",
        "--distribution",
        required=True,
        help="distribution of the repository (e.g. bionic)",
    )
    required.add_argument(
        "-p",
        "--prefix",
        required=True,
        help="prefix of the repository (e.g. schaap)",
    )
    optional.add_argument(
        "-h", "--help", action="help", help="show this help message and exit"
    )
    optional.add_argument(
        "-u",
        "--url",
        default="http://packages.astron.nl:8080",
        help="URL of repository server (default: %(default)s)",
    )
    return parser.parse_args()


class Remover:
    """
    This class contains all functionality to un-publish and remove a Debian
    repository from a Debian package repository server, using the Aptly API.
    """

    def __init__(self, args):
        """
        Initialize a Remover object from the arguments passed in.
        """
        self.args = args
        self.aptly = Client(args.url)

    def _repo_names(self):
        """
        Get a list of the names of the local repositories associated with the
        published repository.
        :return: list of all the local repositories whose name start with
        <prefix>-<distribution>.
        """
        pattern = "-".join((self.args.prefix, self.args.distribution))
        return [
            repo.name
            for repo in self.aptly.repos.list()
            if repo.name.startswith(pattern)
        ]

    def unpublish(self):
        """
        Un-publish the repository on the server. Published repositories are
        identified by their prefix and distribution.
        :return: True if no errors occurred, otherwise False
        """
        success = True
        name = "/".join((self.args.prefix, self.args.distribution))
        try:
            self.aptly.publish.drop(
                prefix=self.args.prefix, distribution=self.args.distribution
            )
            logger.info("Un-published repository %s", name)
        except AptlyAPIException as excp:
            logger.error("Failed to un-publish %s: %s", name, excp)
            success = False
        return success

    def remove(self):
        """
        Remove the local repositories from the server (one for each component).
        Local repositories are named using the following naming convention:
        <prefix>-<distribution>-<component>.
        :return: True if no errors occurred, otherwise False
        """
        success = True
        for repo in self._repo_names():
            try:
                self.aptly.repos.delete(repo)
                logger.info("Removed repository %s", repo)
            except AptlyAPIException as excp:
                logger.error("Failed to remove repository %s: %s", repo, excp)
                success = False
        return success


def main(args):
    """
    Main function. It removes a repository from the repository server, based
    on the command line arguments that were supplied.
    :param args: Arguments as parsed by `parse_arguments()`.
    :return: Error status: 0: success; 1: error
    """
    status = 0
    remover = Remover(args)
    if not remover.unpublish():
        status = 1
    if not remover.remove():
        status = 1
    return status


if __name__ == "__main__":
    sys.exit(main(parse_arguments()))
