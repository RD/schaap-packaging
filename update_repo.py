#!/usr/bin/env python3

"""
Script to update a published Debian repository on a Debian package repository server.
"""

import argparse
import logging
import sys
import textwrap
from aptly_api import Client
from aptly_api.base import AptlyAPIException

logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")
logger = logging.getLogger(__name__)


def parse_arguments():
    """
    Parse command-line arguments.
    :return: argparse.Namespace
    """
    # Do not add help automatically to prevent that two sections of optional arguments appear
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent(
            """
            Update the contents of a published Debian repository on a Debian package
            repository server, using Aptly (https://www.aptly.info). The new packages must
            all be targeted at the same distribution and component. Packages will first
            be uploaded to the local, unpublished repository, whose name will be derived
            from the given prefix, the distribution, and the component. Next, the published
            repository will be updated. For example:

                $ update_repo.py -c testing -d bionic -p schaap everybeam_0.3.0_amd64.deb

            This will first upload the package 'everybeam_0.3.0_amd64.deb' to the local
            repository 'schaap-bionic-testing', and then update the published repository
            'schaap/bionic'. Note that the published repository must support the target
            architecture of the uploaded package(s).
            """
        ),
        add_help=False,
    )
    required = parser.add_argument_group("required arguments")
    optional = parser.add_argument_group("optional arguments")
    required.add_argument(
        "files",
        nargs="+",
        help="white-space separated list of files to upload and publish",
    )
    required.add_argument(
        "-c",
        "--component",
        required=True,
        help="component to publish (e.g. unstable)",
    )
    required.add_argument(
        "-d",
        "--distribution",
        required=True,
        help="name of the distribution (e.g. bionic)",
    )
    required.add_argument(
        "-p", "--prefix", required=True, help="publishing prefix (e.g. schaap)"
    )
    # Add help manually to optional group.
    optional.add_argument(
        "-h", "--help", action="help", help="show this help message and exit"
    )
    optional.add_argument(
        "-s",
        "--skip-signing",
        action="store_true",
        help="skip signing when publishing repository (not recommended)",
    )
    optional.add_argument(
        "-u",
        "--url",
        default="http://packages.astron.nl:8080",
        help="URL of repository server (default: %(default)s)",
    )
    optional.add_argument(
        "--cleanup",
        action="store_true",
        help="cleanup upload directory on the server in case of an error",
    )
    optional.add_argument(
        "--gpg-key", help="GPG key ID to use when signing the release"
    )
    optional.add_argument(
        "--gpg-passphrase",
        help="GPG passphrase to unlock private key (possibly insecure)",
    )
    return parser.parse_args()


class Updater:
    """
    This class contains all functionality to update a published Debian
    repository on a Debian package repository server, using the Aptly API.
    """

    def __init__(self, args):
        self.args = args
        self.aptly = Client(args.url)
        self.reponame = "-".join((args.prefix, args.distribution, args.component))
        self.upload_dir = self.reponame

    def upload(self):
        """
        First, upload file(s) to the server. Next, add the uploaded files to
        the local repository; this will automatically remove them from the
        upload directory.
        :raises: RuntimeError: something went seriously wrong, no files have
                    been uploaded or published;
                 RuntimeWarning: partial success, not all files were
                    successfully uploaded or published.
        """
        # Upload file(s) to the server
        try:
            result = self.aptly.files.upload(self.upload_dir, *self.args.files)
        except AptlyAPIException as excp:
            raise RuntimeError(
                f"Failed to upload files to {self.upload_dir}: {excp}"
            ) from AptlyAPIException
        else:
            logger.info("Uploaded file(s): %s", " ".join(result))

        # Add uploaded files to the local repository; this will automatically
        # remove them from the upload directory
        try:
            result = self.aptly.repos.add_uploaded_file(
                reponame=self.reponame, dir=self.upload_dir, force_replace=True
            )
        except AptlyAPIException as excp:
            raise RuntimeError(
                f"Failed to update local repository: {excp}"
            ) from AptlyAPIException
        else:
            added_files = result.report["Added"]
            removed_files = result.report["Removed"]

            if added_files or removed_files:
                added = f"added: {', '.join(added_files)}"
                removed = f"removed: {', '.join(removed_files)}"
                logger.info("Updated local repository: %s", "; ".join((added, removed)))

            if result.failed_files:
                for warning in result.report["Warnings"]:
                    logger.warning(warning)
                raise RuntimeWarning(
                    f"Failed updating file(s): {', '.join(result.failed_files)}"
                )

    def publish(self):
        """
        Update the published repository with the latest local content.
        :raises RuntimeError: if updating the published repository failed
        """
        try:
            if self.args.skip_signing:
                result = self.aptly.publish.update(
                    distribution=self.args.distribution,
                    prefix=self.args.prefix,
                    sign_skip=True,
                )
            else:
                result = self.aptly.publish.update(
                    distribution=self.args.distribution,
                    prefix=self.args.prefix,
                    sign_gpgkey=self.args.gpg_key,
                    sign_passphrase=self.args.gpg_passphrase,
                )
        except AptlyAPIException as excp:
            raise RuntimeError(
                f"Failed to update published repository: {excp}"
            ) from AptlyAPIException
        else:
            logger.info("Published %s: %s", self.reponame, result)

    def cleanup(self):
        """
        Cleanup the upload directory, if requested by the user
        """
        try:
            self.aptly.files.delete(self.upload_dir)
        except AptlyAPIException as excp:
            logger.warning("Failed to cleanup: %s", excp)


def main(args):
    """
    Main function. It creates a new repository on the repository server, based on the command line
    arguments that were supplied.
    :param args: Arguments as parsed by `parse_arguments()`.
    :return: 0: success;
             1: error: something went seriously wrong, no files have been uploaded or published;
             2: warning: partial success, not all files were successfully uploaded or published.
    """
    updater = Updater(args)
    try:
        updater.upload()
        updater.publish()
        return 0
    except RuntimeError as excp:
        logger.error(excp)
        logger.fatal("Failed to update repository")
        return 1
    except RuntimeWarning as excp:
        logger.warning(excp)
        return 2
    finally:
        if args.cleanup:
            updater.cleanup()


if __name__ == "__main__":
    sys.exit(main(parse_arguments()))
