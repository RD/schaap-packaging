#!/usr/bin/env python3

"""
Script to create and publish a Debian repository on a Debian package repository server.
"""

import argparse
import logging
import sys
import textwrap
from aptly_api import Client
from aptly_api.base import AptlyAPIException

logging.basicConfig(level=logging.INFO, format="%(levelname)s: %(message)s")
logger = logging.getLogger(__name__)


def parse_arguments():
    """
    Parse command-line arguments.
    :return: argparse.Namespace
    """
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=textwrap.dedent(
            """
            Create and publish a Debian repository on a Debian package repository server,
            using Aptly (https://www.aptly.info). The published repository may be multi-
            component, e.g. consisting of 'testing' and 'unstable'. The names of the local,
            unpublished repositories will be derived from the given prefix, the
            distribution, and the components. For example:

                $ create_repo.py -a amd64 -c testing unstable -d bionic -p schaap

            This will produce two local, unpublished repositories: 'schaap-bionic-testing'
            and 'schaap-bionic-unstable' that will contain Debian packages for the amd64
            architecture. These unpublished repositories are only visible to Aptly. The
            local repositories will be published as a single multi-component repository,
            containing amd64 binary packages for the components 'testing' and 'unstable'
            in the distribution 'bionic', using prefix 'schaap'.

            To use the published Debian package repository, the user must create a file on
            his own computer in '/etc/apt/sources.list.d/' (e.g. 'schaap.list'), containing
            either one or both of the following lines:

                deb http://packages.astron.nl/debian/schaap bionic testing
                deb http://packages.astron.nl/debian/schaap bionic unstable

            Next, packages can be installed in the usual way, using 'apt-get update', and
            'apt-get install'.
        """
        ),
        # Do not add help automatically to prevent that two sections of optional arguments appear
        add_help=False,
    )
    required = parser.add_argument_group("required arguments")
    optional = parser.add_argument_group("optional arguments")
    required.add_argument(
        "-a",
        "--architectures",
        nargs="+",
        required=True,
        help="white-space separated list of supported architectures (e.g. amd64)",
    )
    required.add_argument(
        "-c",
        "--components",
        nargs="+",
        required=True,
        help="white-space separated list of components to publish (e.g. unstable)",
    )
    required.add_argument(
        "-d",
        "--distribution",
        required=True,
        help="default distribution when publishing (e.g. bionic)",
    )
    required.add_argument(
        "-p", "--prefix", required=True, help="publishing prefix (e.g. schaap)"
    )
    # Add help manually to optional group.
    optional.add_argument(
        "-h", "--help", action="help", help="show this help message and exit"
    )
    optional.add_argument(
        "-r",
        "--rollback",
        action="store_true",
        help="roll back all changes upon failure",
    )
    optional.add_argument(
        "-s",
        "--skip-signing",
        action="store_true",
        help="skip signing when publishing repository (not recommended)",
    )
    optional.add_argument(
        "-u",
        "--url",
        default="http://packages.astron.nl:8080",
        help="URL of repository server (default: %(default)s)",
    )
    optional.add_argument(
        "--gpg-key", help="GPG key ID to use when signing the release"
    )
    optional.add_argument(
        "--gpg-passphrase",
        help="GPG passphrase to unlock private key (possibly insecure)",
    )
    return parser.parse_args()


class Creator:
    """
    This class contains all functionality to create and publish a Debian
    repository on a Debian package repository server, using the Aptly API.
    """

    def __init__(self, args):
        """
        Initialize a Creator object from the arguments passed in.
        :param args: Arguments as parsed by `parse_arguments()`.
        """
        self.args = args
        self.aptly = Client(args.url)
        self.repositories = [
            {
                "Created": False,
                "Component": component,
                "Distribution": args.distribution,
                "Name": "-".join((args.prefix, args.distribution, component)),
            }
            for component in args.components
        ]

    def create(self):
        """
        Create local repositories on the server, one for each component.
        :return: True if no errors occurred, otherwise False.
        """
        for repo in self.repositories:
            try:
                result = self.aptly.repos.create(
                    reponame=repo["Name"],
                    default_component=repo["Component"],
                    default_distribution=repo["Distribution"],
                )
                repo["Created"] = True
                logger.info("Created repository %s: %s", repo["Name"], result)
            except AptlyAPIException as excp:
                logger.error("Failed to create repository %s: %s", repo["Name"], excp)
        return all(repo["Created"] for repo in self.repositories)

    def publish(self):
        """
        Publish the local repositories on the server.
        :return: True if no errors occurred, otherwise False.
        """
        sources = [{"Name": repo["Name"]} for repo in self.repositories]
        try:
            if self.args.skip_signing:
                result = self.aptly.publish.publish(
                    sources=sources,
                    architectures=self.args.architectures,
                    distribution=self.args.distribution,
                    prefix=self.args.prefix,
                    sign_skip=True,
                )
            else:
                result = self.aptly.publish.publish(
                    sources=sources,
                    architectures=self.args.architectures,
                    distribution=self.args.distribution,
                    prefix=self.args.prefix,
                    sign_gpgkey=self.args.gpg_key,
                    sign_passphrase=self.args.gpg_passphrase,
                    sign_batch=True,
                )
            logger.info("Published repositories: %s", result)
            return True
        except AptlyAPIException as excp:
            logger.error("Failed to publish repositories: %s", excp)
            return False

    def rollback(self):
        """
        Roll back any changes.
        :return: None
        """
        error = False
        for repo in self.repositories:
            if repo["Created"]:
                try:
                    self.aptly.repos.delete(repo["Name"])
                except AptlyAPIException as excp:
                    error = True
                    logger.error("Failed to remove repository %s: %s", repo, excp)
        if error:
            logger.warning("Not all changes were rolled back successfully!")
        else:
            logger.info("Successfully rolled back changes")


def main(args):
    """
    Main function. It creates a new repository on the repository server, based
    on the command line arguments that were supplied.
    :param args: Arguments as parsed by `parse_arguments()`.
    :return: Exit status: 0: success; 1: error
    """
    creator = Creator(args)
    if not creator.create():
        if args.rollback:
            creator.rollback()
        logger.fatal("Failed to create repositories")
        return 1
    if not creator.publish():
        if args.rollback:
            creator.rollback()
        logger.fatal("Failed to publish repository")
        return 1
    return 0


if __name__ == "__main__":
    sys.exit(main(parse_arguments()))
